-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 19, 2022 at 11:08 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `calciumandcomplications`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'neeraj', 'neeraj@coact.co.in', 'hello', '2021-03-09 13:54:26', 'Abbott', 0, 0),
(2, 'Nishanth', 'nishanth@coact.co.in', 'HI', '2021-03-09 14:19:15', 'Abbott', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'neeraj', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-03-10 10:14:47', '2021-03-10 10:14:47', '2021-03-10 16:48:01', 0, 'Abbott'),
(2, 'akshat', 'aj@gmail.com', 'nellore', '9700467764', NULL, NULL, '2021-03-10 10:59:28', '2021-03-10 10:59:28', '2021-03-10 12:31:32', 0, 'Abbott'),
(3, 'akshat', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-03-10 12:31:13', '2021-03-10 12:31:13', '2021-03-10 16:48:01', 0, 'Abbott'),
(4, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'Bangalore', NULL, NULL, '2021-03-10 15:21:20', '2021-03-10 15:21:20', '2021-03-10 20:46:42', 0, 'Abbott'),
(5, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-10 17:25:25', '2021-03-10 17:25:25', '2021-03-10 20:02:06', 0, 'Abbott'),
(6, 'M W APP ', 'mw.app81@gmail.com', 'Mumbai ', 'M W HOSPITAL ', NULL, NULL, '2021-03-10 18:02:25', '2021-03-10 18:02:25', '2021-03-10 18:02:55', 0, 'Abbott'),
(7, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'Bangalore', NULL, NULL, '2021-03-10 18:28:56', '2021-03-10 18:28:56', '2021-03-10 20:46:42', 0, 'Abbott'),
(8, 'Deepak', 'deepak.mondal@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-10 18:48:28', '2021-03-10 18:48:28', '2021-03-10 20:27:54', 0, 'Abbott'),
(9, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-10 18:56:19', '2021-03-10 18:56:19', '2021-03-10 20:02:06', 0, 'Abbott'),
(10, 'Deepak', 'deepak.mondal@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-10 18:57:18', '2021-03-10 18:57:18', '2021-03-10 20:27:54', 0, 'Abbott'),
(11, 'Nilesh chawhan', 'nilesh.chawhan@abbott.com', 'NAGPUR', 'Abbott', NULL, NULL, '2021-03-10 19:01:26', '2021-03-10 19:01:26', '2021-03-10 20:30:30', 0, 'Abbott'),
(12, 'Suniil Hegade', 'suniil.hegade@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-10 19:02:41', '2021-03-10 19:02:41', '2021-03-10 20:30:55', 0, 'Abbott'),
(13, 'Deepak', 'deepak.mondal@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-10 19:06:40', '2021-03-10 19:06:40', '2021-03-10 20:27:54', 0, 'Abbott'),
(14, 'Satish Chirde', 'drsrc1@rediffmail.com', 'yavatmal', 'shri datta hospital yavatmal', NULL, NULL, '2021-03-10 19:08:38', '2021-03-10 19:08:38', '2021-03-10 19:40:01', 0, 'Abbott'),
(15, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'coact', NULL, NULL, '2021-03-10 19:10:01', '2021-03-10 19:10:01', '2021-03-10 20:46:42', 0, 'Abbott'),
(16, 'Amal Vyas', 'amal.vyas@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-10 19:13:01', '2021-03-10 19:13:01', '2021-03-10 19:41:29', 0, 'Abbott'),
(17, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-10 19:13:05', '2021-03-10 19:13:05', '2021-03-10 20:02:06', 0, 'Abbott'),
(18, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-10 19:13:06', '2021-03-10 19:13:06', '2021-03-10 20:02:06', 0, 'Abbott'),
(19, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'Bangalore', NULL, NULL, '2021-03-10 19:14:14', '2021-03-10 19:14:14', '2021-03-10 19:27:40', 0, 'Abbott'),
(20, 'DR SHILPA DESHMUKH KADAM', 'shilpakadam.dr@gmail.com', 'Navi mumbai', 'MGM HOSPITAL', NULL, NULL, '2021-03-10 19:19:03', '2021-03-10 19:19:03', '2021-03-10 20:41:25', 0, 'Abbott'),
(21, 'Krishnakumar G', 'krishnakumar.g@abbott.com', 'Mumbai', 'NA', NULL, NULL, '2021-03-10 19:23:28', '2021-03-10 19:23:28', '2021-03-10 19:45:05', 0, 'Abbott'),
(22, 'Jagadesh Kalathil', 'jagadeesh.kalathil@abbott.com', 'Bangalore East', 'AV', NULL, NULL, '2021-03-10 19:40:48', '2021-03-10 19:40:48', '2021-03-10 20:31:13', 0, 'Abbott'),
(23, 'Jagadesh Kalathil', 'jagadeesh.kalathil@abbott.com', 'Bangalore East', 'AV', NULL, NULL, '2021-03-10 19:41:36', '2021-03-10 19:41:36', '2021-03-10 20:31:13', 0, 'Abbott'),
(24, 'Muhammed Nizar A', 'muhammed.nizar@abbott.com', 'Bangalore ', 'Abbot ', NULL, NULL, '2021-03-10 19:44:59', '2021-03-10 19:44:59', '2021-03-10 20:29:57', 0, 'Abbott'),
(25, 'Zoheb Shaikh', 'zoheb.shaikh@abbott.com', 'Mumbai ', 'Abbott', NULL, NULL, '2021-03-10 19:46:43', '2021-03-10 19:46:43', '2021-03-10 20:07:31', 0, 'Abbott'),
(26, 'Pooja', 'pooja@coact.co.in', 'Mumbai', 'Test', NULL, NULL, '2021-03-10 19:48:07', '2021-03-10 19:48:07', '2021-03-10 20:16:28', 0, 'Abbott'),
(27, 'Ramakrishna Sundaram', 'ramkrishna.sundaram@av.abbott.com', 'Pune', 'Abbott', NULL, NULL, '2021-03-10 19:49:01', '2021-03-10 19:49:01', '2021-03-10 20:06:07', 0, 'Abbott'),
(28, 'Parag Dnyaneshwar Admane', 'drparag_admane@yahoo.co.in', 'Nagpur', 'Spandan', NULL, NULL, '2021-03-10 19:49:45', '2021-03-10 19:49:45', '2021-03-10 20:08:34', 0, 'Abbott'),
(29, 'JAGADEESH KALATHIL', 'jagadeesh.kalathil@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-03-10 19:52:23', '2021-03-10 19:52:23', '2021-03-10 20:31:13', 0, 'Abbott'),
(30, 'Ramkrishna Sundaram', 'ramkrishna.sundaram@abbott.com', 'Pune', 'Abbott Healthcare Pvt Ltd', NULL, NULL, '2021-03-10 19:52:49', '2021-03-10 19:52:49', '2021-03-10 20:29:43', 0, 'Abbott'),
(31, 'Nilesh chawhan', 'nilesh.chawhan@abbott.com', 'NAGPUR', 'Abbott', NULL, NULL, '2021-03-10 19:59:55', '2021-03-10 19:59:55', '2021-03-10 20:30:30', 0, 'Abbott'),
(32, 'JAGADEESH KALATHIL', 'jagadeesh.kalathil@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-03-10 20:01:13', '2021-03-10 20:01:13', '2021-03-10 20:31:13', 0, 'Abbott'),
(33, 'Ramakrishna Sundaram', 'ramkrishna.sundaram@av.abbott.com', 'Pune ', 'Abbott', NULL, NULL, '2021-03-10 20:04:04', '2021-03-10 20:04:04', '2021-03-10 20:06:07', 0, 'Abbott'),
(34, 'Pooja', 'pooja@coact.co.in', 'Mumbai', 'Test', NULL, NULL, '2021-03-10 20:05:20', '2021-03-10 20:05:20', '2021-03-10 20:16:28', 0, 'Abbott'),
(35, 'Muhammed Nizar A', 'muhammed.nizar@abbott.com', 'Bangalore ', 'Abbott ', NULL, NULL, '2021-03-10 20:06:20', '2021-03-10 20:06:20', '2021-03-10 20:29:57', 0, 'Abbott'),
(36, 'Suniil Hegade', 'suniil.hegade@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-10 20:14:12', '2021-03-10 20:14:12', '2021-03-10 20:30:55', 0, 'Abbott');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
