<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_email"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date_new = date('y/m/d');//to verify date
            $user_email=$_SESSION["user_email"];
            $logout_date   = date('Y/m/d H:i:s');
$query_new="SELECT * from tbl_users where user_email='$user_email'";

$res_new = mysqli_query($link, $query_new) or die(mysqli_error($link));
$data = mysqli_fetch_assoc($res_new);


// $data['login_date'];
$day = explode("-",  $data['login_date'] );
$day2 = explode(" ",  $day['2']);
// print_r($day2['0']);
// print_r($day['1']);
// print_r($day['0']);
$new_days = array( '21' , $day['1'],$day2['0']);
$days = implode ("/",$new_days);

           if($days == $logout_date_new){

               
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where user_email='$user_email'  and eventname='$event_name'";
            
            
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

           }
             else{

            
                 $login_date   = date('Y/m/d H:i:s');
                   $logout_date   = date('Y/m/d H:i:s', time() + 30);
                   $user_name = $data['user_name']; 
                   $user_email = $data['user_email']; 

                   $user_city = $data['city']; 
                   $user_hospital = $data['hospital']; 
            

                   $event_name = $data['eventname']; 
    
                  $query="insert into tbl_users(user_name, user_email, city, hospital, login_date, logout_date, joining_date, logout_status, eventname) values('$user_name','$user_email','$user_city','$user_hospital','$login_date','$logout_date','$login_date','0','$event_name')";
                  $res = mysqli_query($link, $query) or die(mysqli_error($link));
                  
                
    

            }
            
            
            
            unset($_SESSION["user_name"]);
            unset($_SESSION["user_email"]);
            
           header("location: index.php");
            exit;
        }

    }

    
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Abbott</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="container-fluid">
    <div class="row logo-nav">
        <div class="col-12 col-md-4">
              <img src="img/abbot_logo.png" class="img-fluid logo" style="max-height:90px;" alt=""/>  
        </div>
        <div class="col-12 col-md-4 offset-md-4 text-right">
           <!-- <img src="img/apdrops.jpg" class="img-fluid logo" alt=""/> -->
        </div>
    </div>
    <div class="row login-info ">
        <div class="col-12 p-1 text-right">
          Hello, <?php echo $_SESSION['user_name']; ?>! <a class="btn btn-sm btn-light" href="?action=logout">Logout</a>
        </div>
    </div>
    <div class="row mt-4">
      <div class="col-12 col-md-7">
            <div class="embed-responsive embed-responsive-16by9">
           <iframe src="https://vimeo.com/event/751017/embed" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe> </div>
        </div>
        <div class="col-12 col-md-5">
          <div id="question" class="mb-3">
              <div id="question-form" class="panel panel-default">
                  <form method="POST" action="#" class="form panel-body" role="form">
                      <div class="row">
                          <div class="col-12">
                       
                          <div id="ques-message"></div>
                          <div class="form-group">
                              <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="5"></textarea>
                          </div>
                          
                          </div>
                          <div class="col-12">
                          <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
                          <input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['user_email']; ?>">
                          <button class="btn btn-primary btn-sm btn-submit" type="submit">Submit your Question</button>
                          </div>
                      </div>
                </form>
              </div>
          </div>
		  <!--
          <center><br>
        <img src="img/logo2.jpg" class="img-fluid" alt=""/> 
        </center>
		-->
        </div>
    </div>
    
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){
    //$('#videolinks').html('If you are unable to watch video, <a href="#" onClick="changeVideo()">click here</a>');
	$(document).on('submit', '#question-form form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#ques-message').text('Your question is submitted successfully.');
                  $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#ques-message').text(data);
                  $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});

function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
         }
});
}
setInterval(function(){ update(); }, 30000);


function changeVideo()
{
    var wc = $('#webcast').attr("src");
    console.log(wc);
    if(wc == "video.php")
    {
        $('#webcast').attr("src","video_bkup.php");
    }
    else
    {
        $('#webcast').attr("src","video.php");
    }
}

</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-13');
</script>

</body>
</html>